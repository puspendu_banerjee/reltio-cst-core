package com.reltio.cst.service.impl;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.RestAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.reltio.cst.properties.AuthenticationProperties.AUTH_SERVER_BASIC_TOKEN;
import static com.reltio.cst.properties.AuthenticationProperties.AUTH_SERVER_HEADER;


public class TokenGeneratorServiceImplTest {

    static final String validAuthUrl = "https://auth.reltio.com/oauth/token";
    private static final String tenantURL = "https://sndbx.reltio.com/reltio/api/yboOEM097UepwC3/entities/";

    /**
     * If you want to locally start test from your Interllij Idea or Eclipse
     * you can transfer these params through JVM params: -Doauth.username=... -Doauth.password=...
     * If you want to start test using maven you should set for maven -DargLine="-Doauth.username=... -Doauth.password=..."
     */
    public static String username = null;
    public static String password = null;
    public static String clientCredentials = null;

    static {
        username = System.getenv("oauthusername2");
        password = System.getenv("oauthpassword2");
        clientCredentials = System.getenv("clientCredentials");

        if (username == null) {
            username = System.getProperty("oauthusername2");
            password = System.getProperty("oauthpassword2");
        }

        if (clientCredentials == null) {
            clientCredentials = System.getProperty("clientCredentials");
        }
    }

    @Test(expected = com.reltio.cst.exception.handler.GenericException.class)
    public void testTokenGneeratorInitiation_FailureWrongUrl()
            throws APICallFailureException, GenericException {
        new TokenGeneratorServiceImpl(username, password,
                "https://auth000.reltio.com/oauth/token");

    }

    @Test(expected = com.reltio.cst.exception.handler.APICallFailureException.class)
    public void testTokenGneeratorInitiation_FailureWrongUsername()
            throws APICallFailureException, GenericException {
        new TokenGeneratorServiceImpl("dummy", password,
                "https://auth.reltio.com/oauth/token");

    }

    @Test
    public void testTokenGeneratorInitiation_success()
            throws APICallFailureException, GenericException {
        TokenGeneratorService tokenGeneratorService = new TokenGeneratorServiceImpl(
                username, password, "https://auth.reltio.com/oauth/token");

        String token = tokenGeneratorService.getToken();
        Assert.assertNotNull(token);
        String newToken = tokenGeneratorService.getNewToken();
        Assert.assertNotNull(newToken);
    }

    @Test
    public void testTokenGeneratorInitiationClientCrdentials_success()
            throws APICallFailureException, GenericException {

        if (clientCredentials != null) {
            TokenGeneratorService tokenGeneratorService = new TokenGeneratorServiceImpl(
                    clientCredentials, "https://auth.reltio.com/oauth/token");

            String token = tokenGeneratorService.getToken();
            Assert.assertNotNull(token);
            String newToken = tokenGeneratorService.getNewToken();
            Assert.assertNotNull(newToken);
        } else {
            assert true;
        }

    }

    @Test
    public void invalidRefreshTokenTest()
            throws APICallFailureException, GenericException {

        Map<String, String> authHeaders = new HashMap<>();
        authHeaders.put(AUTH_SERVER_HEADER, AUTH_SERVER_BASIC_TOKEN);
        RestAPIService apiService = new SimpleRestAPIServiceImpl();

        String authUrl = "https://auth.reltio.com/oauth/token";
        TokenGeneratorServiceImpl tokenGeneratorService = new TokenGeneratorServiceImpl(
                username, password, authUrl);

        tokenGeneratorService.refreshToken();
        String newToken = tokenGeneratorService.getToken();
        System.out.println("Token 1 : " + newToken);
        Assert.assertNotNull(newToken);
        String refreshToken = tokenGeneratorService.getRefreshToken();

        Assert.assertEquals("{\"status\":\"success\"}",
                apiService.post("https://auth.reltio.com/oauth/revoke?token=" + refreshToken,
                        authHeaders, null));
        tokenGeneratorService.refreshToken();
        String newToken2 = tokenGeneratorService.getToken();
        System.out.println("Token 2 : " + newToken2);

        Assert.assertNotSame(newToken, newToken2);

    }

    @Test
    public void getTokenUsingRefreshTokenTest() throws APICallFailureException, GenericException, ReltioAPICallFailureException {
        Map<String, String> authHeaders = new HashMap<>();
        authHeaders.put(AUTH_SERVER_HEADER, AUTH_SERVER_BASIC_TOKEN);
        RestAPIService apiService = new SimpleRestAPIServiceImpl();

        String authUrl = "https://auth.reltio.com/oauth/token";
        TokenGeneratorServiceImpl tokenGeneratorService = new TokenGeneratorServiceImpl(
                username, password, authUrl);

        String newToken = tokenGeneratorService.getToken();
        System.out.println("Token 1 : " + newToken);
        Assert.assertNotNull(newToken);

        Assert.assertEquals("{\"status\":\"success\"}",
                apiService.post("https://auth.reltio.com/oauth/revoke?token=" + newToken,
                        authHeaders, null));

        ReltioAPIService reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
        String response = reltioAPIService.get(tenantURL + "_total");
        System.out.println(response);
        String newToken2 = tokenGeneratorService.getToken();
        System.out.println("Token 2 : " + newToken2);

        Assert.assertNotSame(newToken, newToken2);


    }

}
