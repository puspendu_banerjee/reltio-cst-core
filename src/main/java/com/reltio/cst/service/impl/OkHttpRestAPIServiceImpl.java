package com.reltio.cst.service.impl;

import com.reltio.cst.domain.HttpMethod;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.properties.AuthenticationProperties;
import com.reltio.cst.service.RestAPIService;
import com.reltio.cst.socket.LocalAddressSocketFactory;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.SocketFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OkHttpRestAPIServiceImpl implements RestAPIService {
    public static final Integer DEFAULT_TIMEOUT_IN_MINUTES = 5;
    private static Logger log = LoggerFactory.getLogger(OkHttpRestAPIServiceImpl.class);
    private final OkHttpClient client;

    private final int connectTimeout;

    public OkHttpRestAPIServiceImpl(int timeoutInMinutes) {
        connectTimeout = (int) TimeUnit.MINUTES.toMillis(timeoutInMinutes);
        ConnectionSpec requireTls12 = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .build();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        LocalAddressSocketFactory socketFactory = null;
        try {
            socketFactory = new LocalAddressSocketFactory();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(),e);
        }
        ConnectionPool netConnectionPool=new ConnectionPool(30,1,TimeUnit.MINUTES);
        client =new OkHttpClient().newBuilder()
                .socketFactory(socketFactory)
                .connectionSpecs(Arrays.asList(requireTls12))
                //.addInterceptor(logging)
                //.addNetworkInterceptor(logging)
                .followRedirects(true)
                .followSslRedirects(true)
                .connectTimeout(connectTimeout,TimeUnit.MILLISECONDS)
                .readTimeout(30,TimeUnit.SECONDS)
                .writeTimeout(30,TimeUnit.SECONDS)
                .connectionPool(netConnectionPool)
                .retryOnConnectionFailure(true)
                .build();
        log.info("Initialized OkHttpRestAPIServiceImpl");
    }

    public OkHttpRestAPIServiceImpl() {
        this((int) TimeUnit.MINUTES.toMillis(DEFAULT_TIMEOUT_IN_MINUTES));
    }

    @Override
    public String get(String requestUrl, Map<String, String> requestHeaders) throws APICallFailureException, GenericException {
        return doExecute(requestUrl, requestHeaders, null, HttpMethod.GET);
    }

    @Override
    public String post(String requestUrl, Map<String, String> requestHeaders, String requestBody) throws APICallFailureException, GenericException {
        return doExecute(requestUrl, requestHeaders, requestBody, HttpMethod.POST);
    }

    @Override
    public String put(String requestUrl, Map<String, String> requestHeaders, String requestBody) throws APICallFailureException, GenericException {
        return doExecute(requestUrl, requestHeaders, requestBody, HttpMethod.PUT);
    }

    @Override
    public String delete(String requestUrl, Map<String, String> requestHeaders, String requestBody) throws APICallFailureException, GenericException {
        return doExecute(requestUrl, requestHeaders, requestBody, HttpMethod.DELETE);
    }

    @Override
    public String doExecute(String requestUrl, Map<String, String> requestHeaders, String requestBody, HttpMethod requestMethod) throws APICallFailureException, GenericException {
        Request.Builder requestBuilder=new Request.Builder()
                .url(requestUrl);
        if(requestBody!=null) {
            requestBuilder = requestBuilder.method(requestMethod.name(),
                    RequestBody.create(requestBody,MediaType.parse(
                            requestHeaders.getOrDefault("Content-Type","text/plain;charset=UTF-8"))));
            requestHeaders.remove("Content-Type");
        }else{
            requestBuilder.method(requestMethod.name(),null);
        }
        for(Map.Entry<String, String> x : requestHeaders.entrySet()){
            requestBuilder=requestBuilder.addHeader(x.getKey(),x.getValue());
        };
        Request request=requestBuilder.build();
        String res=null;
        //log.debug("network Connection pool Size: "+client.connectionPool().connectionCount());
        Call restCall=client.newCall(request);
        try (Response response = restCall.execute()){
            // Verify the Response
            int responseCode = response.code();
            if (responseCode >= 400) {
                    throw new APICallFailureException(responseCode,
                            responseCode==400? AuthenticationProperties.INVALID_REFRESH_TOKEN_ERROR:response.message());
                }
            res=response.body().string();
            response.body().close();
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(),e);
            throw new GenericException(e.getMessage());
        }
        return res;
    }
}

