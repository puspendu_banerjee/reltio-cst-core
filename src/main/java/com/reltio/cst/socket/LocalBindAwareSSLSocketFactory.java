package com.reltio.cst.socket;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.net.*;
import java.security.NoSuchAlgorithmException;

import static com.reltio.cst.socket.SocketUtiil.*;

public class LocalBindAwareSSLSocketFactory extends SSLSocketFactory {
    private SSLContext sslContext;
    private SSLSocketFactory sslSocketFactory;
    private InetAddress localAddress;
    private InetSocketAddress inetSocketAddress;

    private static final Logger LOGGER= LoggerFactory.getLogger(LocalBindAwareSSLSocketFactory.class);

    public LocalBindAwareSSLSocketFactory() throws NoSuchAlgorithmException, UnknownHostException, SocketException {
        this(SSLContext.getDefault());
    }

    public LocalBindAwareSSLSocketFactory(final SSLContext sslContext) throws NoSuchAlgorithmException, UnknownHostException, SocketException {
        this.sslContext=sslContext;
        this.sslSocketFactory= sslContext.getSocketFactory();
        this.localAddress=getBindAddress();
        this.inetSocketAddress=new InetSocketAddress(localAddress,0);
        LOGGER.info("Instance Created with localAddress bound to : "+localAddress);
    }

    @Override
    public Socket createSocket() throws  IOException{
        Socket unconnectedSocket= sslSocketFactory.createSocket();
        unconnectedSocket.bind(inetSocketAddress);
        return unconnectedSocket;
    }

    @Override
    public Socket createSocket(String host, int port) throws IOException {
        Socket sslSocket=sslSocketFactory.createSocket(host,port,localAddress,0 );
        return sslSocket;
    }

    @Override
    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
        return createSocket(InetAddress.getByName(host),port,localHost,0);
    }

    @Override
    public Socket createSocket(InetAddress host, int port) throws IOException {
        return createSocket(host,port,localAddress,0);
    }

    @Override
    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        return sslSocketFactory.createSocket(address,port,localAddress,localPort);
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return sslSocketFactory.getDefaultCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return sslSocketFactory.getSupportedCipherSuites();
    }

    @Override
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        if(!s.isBound()) {
            s.bind(inetSocketAddress);
        }
        return sslSocketFactory.createSocket(s, host, port, autoClose);
    }

}
