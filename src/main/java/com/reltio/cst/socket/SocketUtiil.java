package com.reltio.cst.socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Enumeration;

public class SocketUtiil {
    private static final Logger LOGGER= LoggerFactory.getLogger(SocketUtiil.class);

    public static InetAddress getBindAddress() throws SocketException, UnknownHostException {
        String ipAddressOrHost=System.getProperty("ssl.socket.bind.address");
        String interfaceName=System.getProperty("ssl.socket.bind.if.name");
        String hwAddr=System.getProperty("ssl.socket.bind.mac");
        String interfaceDisplayName=System.getProperty("ssl.socket.bind.if.displayname");
        InetAddress bindAddress= getBindAddress(ipAddressOrHost,interfaceName,hwAddr,interfaceDisplayName);
        LOGGER.debug("Binding Address: "+bindAddress.getHostAddress());
        return bindAddress;
    }

    public static InetAddress getBindAddress(String ipAddressOrHost, String interfaceName, String hwAddr, String interfaceDisplayName) throws SocketException, UnknownHostException {
        if(LOGGER.isTraceEnabled()) {
            displayAllInterfaceInformation();
        }
        if (ipAddressOrHost!=null){return InetAddress.getByName(ipAddressOrHost);}
        if(interfaceName!=null) {
            return NetworkInterface.getByName(interfaceName).getInetAddresses().nextElement();
        }
        if(hwAddr!=null){
            NetworkInterface matchedNetworkInterface = Collections.list(NetworkInterface.getNetworkInterfaces())
                    .stream()
                    .filter(networkInterface ->
                            !hwAddr.equalsIgnoreCase(getMACAddress(networkInterface)))
                    .findAny().orElse(null);
            return matchedNetworkInterface!=null?
                    matchedNetworkInterface.getInetAddresses().nextElement():null;
        }
        return InetAddress.getLocalHost();
    }

    public static String getMACAddress(NetworkInterface networkInterface){
        try {
            byte [] mac=networkInterface.getHardwareAddress();
            String mACStr=mACBytesToString(mac);
            LOGGER.trace("MAC Address: "+ mACStr);
            return mACStr;
        } catch (SocketException e) {
            LOGGER.error(e.getLocalizedMessage(),e);
        }
        return null;
    }

    public static String mACBytesToString(final byte[] mac){
        if(null==mac){//Virtual Interfaces could have null hardware address
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mac.length; i++) {
            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
        }
        return sb.toString();
    }

    public static void displayAllInterfaceInformation() throws SocketException {
        for(NetworkInterface networkInterface:Collections.list(NetworkInterface.getNetworkInterfaces())){
            displayInterfaceInformation(networkInterface);
        }
    }

    private static void displayInterfaceInformation(final NetworkInterface netint) throws SocketException {
        LOGGER.debug(String.format("\nDisplay name: %s\nName: %s\n", netint.getDisplayName(),netint.getName()));
        Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();

        for (InetAddress inetAddress : Collections.list(inetAddresses)) {
            LOGGER.trace(String.format("\tInetAddress: %s\n", inetAddress));
        }

        LOGGER.trace(String.format(
                "\nUp? %s\nLoopback? %s\nPointToPoint? %s\nMulticast? %s\nVirtual? %s\nHardware address: %s\nMTU: %s\n",
                netint.isUp(),netint.isLoopback(),netint.isPointToPoint(),netint.supportsMulticast(),
                netint.isVirtual(),mACBytesToString(netint.getHardwareAddress()),netint.getMTU()));
    }
}
