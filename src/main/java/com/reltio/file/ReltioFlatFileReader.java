package com.reltio.file;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.regex.Pattern;

public class ReltioFlatFileReader implements ReltioFileReader, AutoCloseable {

    private static Logger logger = LoggerFactory.getLogger(ReltioFlatFileReader.class.getName());
    private final BufferedReader fileReader;
    private final String separator;

    public ReltioFlatFileReader(String fileName, String separator) throws Exception {
        CharsetMatch cm = getCharsetMatch(fileName);

        this.fileReader = getFileReader(fileName, cm);
        // BOM marker will only appear on the very beginning
        skipBOMMarker();
        this.separator = Pattern.quote(separator);
    }

    public ReltioFlatFileReader(String fileName) throws IOException {
        CharsetMatch cm = getCharsetMatch(fileName);

        this.fileReader = getFileReader(fileName, cm);
        // BOM marker will only appear on the very beginning
        skipBOMMarker();
        this.separator = null;
    }

    public ReltioFlatFileReader(String fileName, String separator, String decoder) throws IOException {
        CharsetDecoder newDecoder = Charset.forName(decoder).newDecoder();
        newDecoder.onMalformedInput(CodingErrorAction.REPLACE);
        fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(StringEscapeUtils.escapeJava(fileName)), newDecoder));
        // BOM marker will only appear on the very beginning
        skipBOMMarker();

        if (separator != null) {
            this.separator = Pattern.quote(separator);
        } else {
            this.separator = null;
        }
    }

    private BufferedReader getFileReader(String fileName, CharsetMatch cm) throws FileNotFoundException {
        BufferedReader result;
        if (Charset.availableCharsets().get(cm.getName()) == null
                || cm.getName().equals("Shift_JIS")) {
            logger.info("The "
                    + cm.getName()
                    + " charset not supported. So letting the reader to choose apporiate the Charset...");
            return new BufferedReader(new InputStreamReader(new FileInputStream(StringEscapeUtils.escapeJava(fileName))));
        } else {
            CharsetDecoder newdecoder = Charset.forName(cm.getName()).newDecoder();
            newdecoder.onMalformedInput(CodingErrorAction.REPLACE);

            return new BufferedReader(new InputStreamReader(new FileInputStream(StringEscapeUtils.escapeJava(fileName)), newdecoder));
        }
    }

    private CharsetMatch getCharsetMatch(String fileName) throws IOException {
        CharsetMatch cm;
        try (BufferedInputStream isr = new BufferedInputStream(new FileInputStream(StringEscapeUtils.escapeJava(fileName)))) {
            CharsetDetector charsetDetector = new CharsetDetector();
            charsetDetector.setText(isr);
            charsetDetector.enableInputFilter(true);
            cm = charsetDetector.detect();
            logger.info("Decorder of the" + fileName + "(CharSet) :: " + cm.getName());
        }
        return cm;
    }

    @Override
    public String[] readLine() throws IOException {
        String line = fileReader.readLine();
        if (line != null) {
            if (line != null && !line.trim().isEmpty()) {
                if (separator != null) {
                    return line.split(separator, -1);
                } else {
                    String[] strArray = new String[1];
                    strArray[0] = line;
                    return strArray;
                }

            }
        }

        return null;
    }

    @Override
    public void close() throws IOException {
        fileReader.close();
    }

    private void skipBOMMarker() {
        try {
            fileReader.mark(4);
            if ('\ufeff' != fileReader.read())
                fileReader.reset(); // not the BOM marker
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
